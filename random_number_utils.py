# -*- coding: utf-8 -*-
"""
Created on Sun Nov 17 22:15:54 2019

@author: bhaskara rao kandi

This program is to generate normally distributed random numbers

"""
import numpy as np
def normal_rvs(no_of_rands,mu=0,sigma=1,is_stnd_normal = False):
    """
    This function generates rvs from normal distribution
    Args:
        no_of_rands: number of random variables you want to generate
        mu : mean of the normal distribution
        sigma : standard deviation of the normal distribution
        is_stnd_normal: Do you want to generate from standard normal
                        distribution or not
    Returns: 
           numpy array: This return numpy array of size  no_of_rands                
                        
    """
    if not is_stnd_normal:
       return np.random.normal(mu, sigma, no_of_rands)
    else:
       return np.random.normal(0, 1, no_of_rands)

def poisson_rvs(no_of_rands, mean):
    """
    This function generates rvs from normal distribution
    Args:
        no_of_rands: number of random variables you want to generate
        mean: average of the poisson distribution
    Returns: 
           numpy array: This return numpy array of size  no_of_rands                
                        
    """
    return np.random.poisson(mean, no_of_rands)